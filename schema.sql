
DROP TABLE IF EXISTS positions;

CREATE TABLE positions (
  id serial,
  latitude double precision not null,
  longitude double precision not null,
  datetime timestamp,
  primary key(id));

--INSERT INTO positions (latitude, longitude, datetime)
--  VALUES(42.308693, -83.063992, '2015-03-04 02:55:57');